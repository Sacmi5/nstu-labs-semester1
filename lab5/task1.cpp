#include <stdio.h>
#include <stdbool.h>
#include <locale.h>

#define N 256

/* Функция из набора. Изменена выводимая информация, для удобства пользователя */
void Enter_Matrix(int Array[N][N], int &m, int &n)
{
	int i, j, t;

	while (true)
	{
		printf_s("Введите размерности матрицы (необходимо использовать натуральные числа, в качестве разделителя используйте пробел или Enter, максимум %d): ", N);
		t = scanf_s("%d %d", &m, &n);

		if (t == 2 && m < N && n < N && m > 0 && n > 0)
			break;

		printf_s("Извините, но данные некорректны. Попробуйте еще раз.\n");
	}

	printf_s("Введите элементы матрицы:\n");

	for (i = 0; i < m; i++)
		for (j = 0; j < n; j++)
			scanf_s("%d", &Array[i][j]);
}

void get_elements(int array[N][N], int m, int n)
{
	int k = 0;
	bool is_special;

	printf_s("Вывод чисел: ");

	for (size_t i = 0; i < m; i++)
		for (size_t j = 0; j < n; j++)
		{
			is_special = true;

			for (size_t l = 0; l < j; l++)
			{
				if (array[i][l] >= array[i][j])
				{
					is_special = false;
					break;
				}
			}

			if (is_special)
				for (size_t l = j + 1; l < n; l++)
				{
					if (array[i][l] <= array[i][j])
					{
						is_special = false;
						break;
					}
				}

			if (is_special)
			{
				k += 1;
				printf_s("%d ", array[i][j]);
			}
		}

	if (!k)
		printf_s("Нет таких чисел, которые удовлетворяют условию");
	else
		printf_s("\nКоличество чисел, удовлетворяющие условию: %d", k);
}

int main()
{
	int array[N][N], m, n;

	setlocale(LC_ALL, "Russian");

	Enter_Matrix(array, m, n);
	get_elements(array, m, n);

	return 0;
}