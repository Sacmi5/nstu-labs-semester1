#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>

#define N 100
#define _countof(array) (sizeof(array) / sizeof(array[0]))

void removeDuplicate(char source[N], char result[N])
{
	char letter;
	int counter, letter_pos, length;

	length = strlen(source);

	if (length < 2)
		return;

	for (size_t i = length - 1; i > 0; i--)
		if (!(ispunct(source[i] || isdigit(source[i]))))
		{
			letter = source[i];
			letter_pos = i;
			break;
		}

	if (!letter)
		return;

	counter = 0;

	for (size_t i = 0; i < length; i++)
		if ((source[i] != letter) || (letter_pos == i))
			result[i - counter] = source[i];
		else
			counter += 1;

	result[length - counter] = '\0';
}

int main1()
{
	char str[N], edited_str[N];

	setlocale(LC_ALL, "Russian");

	printf_s("Введите строку (максимум символов: %d): ", N);
	fgets(str, N, stdin);

	removeDuplicate(str, edited_str);

	printf_s("Результат: %s", edited_str);

	return 0;
}