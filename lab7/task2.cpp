#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include <stdbool.h>

char *getString(int &length)
{
    char *string;
    int scan; // поменять название

    while (true)
    {
        printf_s("Введите длину строки: ", &length);
        scan = scanf_s("%d", &length);

        if (length > 0 && scan == 1)
            break;

        printf_s("Вы ввели неверные данные. Попробуйте еще раз.\n");
        while (getchar() != '\n')
            ; // чистка буфера
    }

    string = (char *)malloc(sizeof(char) * length);
    while (getchar() != '\n')
        ; // чистка буфера

    printf_s("Введите строку: ");
    fgets(string, length, stdin);

    return string;
}

char *searchWordsAndEdit(char *string, int length)
{
    int from_pos = 0;
    int count = 0;
    char *rev_string, *new_string;
    char letter;
    bool is_letter = false;

    rev_string = strdup(string);
    new_string = strdup(string);
    strrev(rev_string);
    *rev_string++;

    new_string = (char *)malloc(sizeof(char) * length);

    while (*string != '\n')
    {
        if (!isspace(*rev_string))
        {
            if (!is_letter)
                if (!(isdigit(*rev_string) || ispunct(*rev_string)))
                {
                    letter = *rev_string;
                    is_letter = true;
                    *new_string = letter;
                    *new_string++;
                }

            if (*rev_string != letter)
            {
                *new_string = *rev_string;
                *new_string++;
            }
        }
        else
        {
            is_letter = false;
            *new_string = ' ';
            *new_string++;
        }

        *rev_string++;
        *string++;
    }

    strrev(new_string);
    return new_string;
}

int main()
{
    char *ptrIn, *ptrOut;
    int length;

    setlocale(LC_ALL, "Russian");
    ptrIn = getString(length);
    ptrOut = searchWordsAndEdit(ptrIn, length);

    return 0;
}